from netmiko import ConnectHandler, NetMikoTimeoutException, NetMikoAuthenticationException
from ttp import ttp
from getpass import getpass
import csv


def main():
    power_inline_result =[]
    power_inline_all= []
    with open('templates/power_inline_noheader.ttp','r') as r:
        ttp_template = r.read()    
    # with open('switch_list.csv') as csv_file:
    #     csv_reader = csv.DictReader(csv_file)
    #     for row in csv_reader:       
    #         switch_hostname =row['switch_hostname']
    switch_hostname =""
    switch_username=""
    switch_password =""
    sw_connection_failed = True
    print("\n")
    print("*** Enter Switch details or type bye to exit *** \n")
    print()
    while sw_connection_failed: 
        #Switch hostname
        while not switch_hostname:       
            switch_hostname = input("Enter switch Hostname or IP Address: ")
            if switch_hostname.lower() =="bye" :
                return
        #Switch username
        while not switch_username:       
            switch_username = input("Enter Switch Username: ")
            if switch_username.lower() =="bye" :
                return
        #Switch password
        while not switch_password:
            switch_password = getpass("Enter Switch Password: ")
            if switch_password.lower() =="bye" :
                return
        print()  
        # Connect to Switch           
        my_device = {
            'host': switch_hostname,
            'username': switch_username,
            'password': switch_password,
            'device_type': 'cisco_ios'
            }
        try:
            net_connect = ConnectHandler(**my_device)
        except NetMikoAuthenticationException:
            print('!!! Invalid Username or Password, please try again !!!\n')
            switch_username =""
            switch_password =""
            continue
        except NetMikoTimeoutException:
            print('!!! {} is not reachable, please try again !!!\n'.format(switch_hostname)) 
            switch_hostname =""
            continue
        except Exception as e:
            print('!!! Error occurred when connecting to {} : {} !!!\n'.format(switch_hostname, e))
            continue
        power_inline=net_connect.send_command("show power inline")
        net_connect.disconnect()
        power_inline_ttp = ttp(data=power_inline, template=ttp_template)
        power_inline_ttp.parse()

        power_inline_result.append({'switch_hostname':switch_hostname, 'power_output':power_inline_ttp.result()[0][0]})
        sw_connection_failed = False
    
    for item in power_inline_result:
        if type(item['power_output']) is dict :
            item['power_output']['switch_hostname'] = item['switch_hostname']
            power_inline_all.append(item['power_output'])
        else:
            for idx, dicti in enumerate(item['power_output']):
                dicti['switch_hostname']= item['switch_hostname'] + "-S" + str(idx+1)
                power_inline_all.append(dicti)

    with open('{}_power_consumption.csv'.format(switch_hostname),'w') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames= ["power_module","power_available","power_used","power_remaining", "switch_hostname"], delimiter=',', lineterminator='\n')
        csv_writer.writeheader() 
        for line in power_inline_all:                      
            csv_writer.writerow(line)
                
    print("!!! power consumption details exported to {}_power_consumption.csv !!! ".format(switch_hostname))
 

if __name__ == "__main__":
    main()

